﻿using UnityEngine;
using System.Collections;

public static class InputManager
{
    static InputManager()
    {
        Enabled = true;
    }

    public static bool Enabled { get; set; }

    public static float GetAxis(string axis)
    {
        if (!Enabled)
        {
            return 0;
        }

        return Input.GetAxis(axis);
    }

    public static bool GetKey(string name)
    {
        if (!Enabled)
        {
            return false;
        }

        return Input.GetKey(name);
    }

    public static bool GetKeyDown(string name)
    {
        if (!Enabled)
        {
            return false;
        }

        return Input.GetKeyDown(name);
    }

    public static bool GetKeyUp(string name)
    {
        if (!Enabled)
        {
            return false;
        }

        return Input.GetKeyUp(name);
    }
}
