var mass = 1.00;
var wheelRadius = 0.00;
var suspensionRange = 0.00;
var suspensionForce = 0.00;
var suspensionDamp = 0.00;
var compressionFrictionFactor = 0.00;

var sidewaysFriction = 0.00;
var sidewaysDamp = 0.00;
var sidewaysSlipVelocity = 0.00;
var sidewaysSlipForce = 0.00;
var sidewaysSlipFriction = 0.00; 
var sidewaysStiffnessFactor = 0.00;

var forwardFriction = 0.00;
var forwardSlipVelocity = 0.00;
var forwardSlipForce = 0.00;
var forwardSlipFriction = 0.00;
var forwardStiffnessFactor = 0.00; 

var frictionSmoothing = 1.00;

private var hit : RaycastHit;

private var parent : Rigidbody;
parent = transform.root.rigidbody;

private var graphic : Transform;
graphic = transform.FindChild("Graphic");

private var wheelCircumference = 0.00;
wheelCircumference = wheelRadius * Mathf.PI * 2;

private var usedSideFriction = 0.00;
private var usedForwardFriction = 0.00;
private var sideSlip = 0.00;
private var forwardSlip = 0.00;

var car : VehiclePhysics;
var driven = false;

var speed = 0.00;

var brake = false;
var skidbrake = false;

function FixedUpdate ()
{
	down = transform.TransformDirection(Vector3.down); 
	if(Physics.Raycast (transform.position, down, hit, suspensionRange + wheelRadius) && hit.collider.transform.root != transform.root)
	{
		velocityAtTouch = parent.GetPointVelocity(hit.point);
		
		compression = hit.distance / (suspensionRange + wheelRadius);
		compression = -compression + 1;
		force = -down * compression * suspensionForce;
		
		t = transform.InverseTransformDirection(velocityAtTouch);
		t.z = t.x = 0;
		shockDrag = transform.TransformDirection(t) * -suspensionDamp;
		
		forwardDifference = transform.InverseTransformDirection(velocityAtTouch).z - speed;
		newForwardFriction = Mathf.Lerp(forwardFriction, forwardSlipFriction, forwardSlip * forwardSlip);
		newForwardFriction = Mathf.Lerp(newForwardFriction, newForwardFriction * compression * 1, compressionFrictionFactor);
		if(frictionSmoothing > 0)
			usedForwardFriction = Mathf.Lerp(usedForwardFriction, newForwardFriction, Time.fixedDeltaTime / frictionSmoothing);
		else
			usedForwardFriction = newForwardFriction;
		forwardForce = transform.TransformDirection(Vector3(0, 0, -forwardDifference)) * usedForwardFriction;
		forwardSlip = Mathf.Lerp(forwardForce.magnitude / forwardSlipForce, forwardDifference / forwardSlipVelocity, forwardStiffnessFactor);
		
		sidewaysDifference = transform.InverseTransformDirection(velocityAtTouch).x;
		newSideFriction = Mathf.Lerp(sidewaysFriction, sidewaysSlipFriction, sideSlip * sideSlip);
		newSideFriction = Mathf.Lerp(newSideFriction, newSideFriction * compression * 1, compressionFrictionFactor);
		if(frictionSmoothing > 0)
			usedSideFriction = Mathf.Lerp(usedSideFriction, newSideFriction, Time.fixedDeltaTime / frictionSmoothing);
		else
			usedSideFriction = newSideFriction;
		sideForce = transform.TransformDirection(Vector3(-sidewaysDifference, 0, 0)) * usedSideFriction;
		sideSlip = Mathf.Lerp(sideForce.magnitude / sidewaysSlipForce, sidewaysDifference / sidewaysSlipVelocity, sidewaysStiffnessFactor);
		
		t = transform.InverseTransformDirection(velocityAtTouch);
		t.z = t.y = 0;
		sideDrag = transform.TransformDirection(t) * -sidewaysDamp;
		
		parent.AddForceAtPosition(force + shockDrag + forwardForce + sideForce + sideDrag, hit.point);

		if(driven)
			car.AddForceOnMotor (forwardDifference * usedForwardFriction * Time.fixedDeltaTime);
		else
			speed += forwardDifference;	

		graphic.position = transform.position + (down * (hit.distance - wheelRadius));
	}
	else
	{
		graphic.position = transform.position + (down * suspensionRange);
	}
	
	graphic.transform.Rotate(360 * (speed / wheelCircumference) * Time.fixedDeltaTime, 0, 0); 
}

