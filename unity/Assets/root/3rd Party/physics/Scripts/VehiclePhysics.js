var wheelFR : Transform;
var wheelBL : Transform;
var wheelBR : Transform;

var frontWheelSteering = false;
var rearWheelSteering = false;

var frontWheelDrive = false;
var rearWheelDrive = false;

var minSteeringAngle = 0.00;
var maxSteeringAngle = 0.00;
var highSpeed = 0.00;

var motorMass = 1.00;
var motorDrag = 0.00;
var maxAcceleration = 0.00;
var maxMotorSpeed = 0.00;
var minTorque = 0.00;
var maxTorque = 0.00;

var centerOfMass : Vector3;
rigidbody.centerOfMass = centerOfMass;


private var scriptWheelFR : WheelPhysics;
private var scriptWheelBL : WheelPhysics;
private var scriptWheelBR : WheelPhysics;

scriptWheelFR = wheelFR.GetComponent(WheelPhysics);
scriptWheelBL = wheelBL.GetComponent(WheelPhysics);
scriptWheelBR = wheelBR.GetComponent(WheelPhysics);

scriptWheelFR.car = this;
scriptWheelBL.car = this;
scriptWheelBR.car = this;

var steeringInput = 0.00;
var motorInput = 0.00;

var motorTorque = 0.00;
var motorAccel = 0.00;
var motorSpeed = 0.00;
var steeringAngle = 0.00;

if(frontWheelDrive) scriptWheelFR.driven = true;
if(rearWheelDrive) scriptWheelBL.driven = scriptWheelBR.driven = true;

function FixedUpdate ()
{
	steeringInput = InputManager.GetAxis("Horizontal");
	motorInput = InputManager.GetAxis("Vertical");
	
	if(frontWheelSteering) wheelFR.localRotation = Quaternion.LookRotation(Vector3(steeringInput * (steeringAngle / 90), 0, 1 + (-1 * Mathf.Abs(steeringInput * (steeringAngle / 90))) ));
	if(rearWheelSteering) wheelBL.localRotation = wheelBR.localRotation = Quaternion.LookRotation(Vector3(-steeringInput * (steeringAngle / 90), 0, 1 + (-1 * Mathf.Abs(steeringInput * (steeringAngle / 90))) ));
	
	if(frontWheelDrive || rearWheelDrive)
	{
		motorTorque = Mathf.Lerp(maxTorque / 30, minTorque / 30, motorSpeed / maxMotorSpeed) * Mathf.Abs(motorInput);
		if(motorTorque < 1) motorTorque = 1;
		motorAccel = Mathf.Lerp(maxAcceleration, 0, motorSpeed / maxMotorSpeed);
		steeringAngle = Mathf.Lerp(maxSteeringAngle, minSteeringAngle, rigidbody.velocity.magnitude / highSpeed);

		motorSpeed += motorInput * motorAccel / motorMass * Time.fixedDeltaTime;
		
		if(frontWheelDrive)
		{
			scriptWheelFR.speed = motorSpeed;
		}
		
		if(rearWheelDrive)
		{
			scriptWheelBL.speed = scriptWheelBR.speed = motorSpeed;
		}
		
		motorSpeed += -motorSpeed * motorDrag / motorTorque * Time.fixedDeltaTime;
	}
} 

function AddForceOnMotor (force : float)
{
	motorSpeed += force / motorTorque;
}