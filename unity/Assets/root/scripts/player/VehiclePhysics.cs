﻿using UnityEngine;
using System.Collections;

public class VehiclePhysics : MonoBehaviour {
	public Transform wheelFR;
	public Transform wheelBL;
	public Transform wheelBR;
	
	public bool frontWheelSteering= false;
	public bool rearWheelSteering= false;
	
	public bool frontWheelDrive= false;
	public bool rearWheelDrive= false;
	
	public float minSteeringAngle= 0.00f;
	public float maxSteeringAngle= 0.00f;
	public float highSpeed= 0.00f;
	
	public float motorMass= 1.00f;
	public float motorDrag= 0.00f;
	public float maxAcceleration= 0.00f;
	public float maxMotorSpeed= 0.00f;
	public float minTorque= 0.00f;
	public float maxTorque= 0.00f;
	
	public Vector3 centerOfMass;

	private WheelPhysics scriptWheelFR;
	private WheelPhysics scriptWheelBL;
	private WheelPhysics scriptWheelBR;

	public float steeringInput= 0.00f;
	public float motorInput= 0.00f;
	
	public float motorTorque= 0.00f;
	public float motorAccel= 0.00f;
	public float motorSpeed= 0.00f;
	public float steeringAngle= 0.00f;

	void Start(){
		rigidbody.centerOfMass = centerOfMass;

		scriptWheelFR = wheelFR.GetComponent<WheelPhysics>();
		scriptWheelBL = wheelBL.GetComponent<WheelPhysics>();
		scriptWheelBR = wheelBR.GetComponent<WheelPhysics>();

		scriptWheelFR.car = this;
		scriptWheelBL.car = this;
		scriptWheelBR.car = this;

		if(frontWheelDrive) scriptWheelFR.driven = true;
		if(rearWheelDrive) scriptWheelBL.driven = scriptWheelBR.driven = true;
	}

	void FixedUpdate (){
		steeringInput = InputManager.GetAxis("Horizontal");
		motorInput = InputManager.GetAxis("Vertical");
		
		if(frontWheelSteering) wheelFR.localRotation = Quaternion.LookRotation(new Vector3(steeringInput * (steeringAngle / 90), 0, 1 + (-1 * Mathf.Abs(steeringInput * (steeringAngle / 90))) ));
		if(rearWheelSteering) wheelBL.localRotation = wheelBR.localRotation = Quaternion.LookRotation(new Vector3(-steeringInput * (steeringAngle / 90), 0, 1 + (-1 * Mathf.Abs(steeringInput * (steeringAngle / 90))) ));
		
		if(frontWheelDrive || rearWheelDrive)
		{
			motorTorque = Mathf.Lerp(maxTorque / 30, minTorque / 30, motorSpeed / maxMotorSpeed) * Mathf.Abs(motorInput);
			if(motorTorque < 1) motorTorque = 1;
			motorAccel = Mathf.Lerp(maxAcceleration, 0, motorSpeed / maxMotorSpeed);
			steeringAngle = Mathf.Lerp(maxSteeringAngle, minSteeringAngle, rigidbody.velocity.magnitude / highSpeed);
			
			motorSpeed += motorInput * motorAccel / motorMass * Time.fixedDeltaTime;
			
			if(frontWheelDrive)
			{
				scriptWheelFR.speed = motorSpeed;
			}
			
			if(rearWheelDrive)
			{
				scriptWheelBL.speed = scriptWheelBR.speed = motorSpeed;
			}
			
			motorSpeed += -motorSpeed * motorDrag / motorTorque * Time.fixedDeltaTime;
		}
	} 
	
	public void  AddForceOnMotor ( float force  ){
		motorSpeed += force / motorTorque;
	}
}