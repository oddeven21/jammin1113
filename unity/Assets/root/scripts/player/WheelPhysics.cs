﻿
using UnityEngine;
using System.Collections;

public class WheelPhysics : MonoBehaviour {
	public float mass= 1.00f;
	public float wheelRadius= 0.00f;
	public float suspensionRange= 0.00f;
	public float suspensionForce= 0.00f;
	public float suspensionDamp= 0.00f;
	public float compressionFrictionFactor= 0.00f;
	
	public float sidewaysFriction= 0.00f;
	public float sidewaysDamp= 0.00f;
	public float sidewaysSlipVelocity= 0.00f;
	public float sidewaysSlipForce= 0.00f;
	public float sidewaysSlipFriction= 0.00f; 
	public float sidewaysStiffnessFactor= 0.00f;
	
	public float forwardFriction= 0.00f;
	public float forwardSlipVelocity= 0.00f;
	public float forwardSlipForce= 0.00f;
	public float forwardSlipFriction= 0.00f;
	public float forwardStiffnessFactor= 0.00f; 
	
	public float frictionSmoothing= 1.00f;
	
	private RaycastHit hit;
	
	private Rigidbody parent;
	
	private Transform graphic;
	
	private float wheelCircumference= 0.00f;
	
	private float usedSideFriction= 0.00f;
	private float usedForwardFriction= 0.00f;
	private float sideSlip= 0.00f;
	private float forwardSlip= 0.00f;
	
	public VehiclePhysics car;
	public bool driven= false;
	
	public float speed= 0.00f;
	
	public bool brake= false;
	public bool skidbrake= false;

	void Start(){
		parent = transform.root.rigidbody;

		graphic = transform.FindChild("Graphic");

		wheelCircumference = wheelRadius * Mathf.PI * 2;
	}
	
	void FixedUpdate (){
		Vector3 down = transform.TransformDirection(Vector3.down); 
		if(Physics.Raycast (transform.position, down, out hit, suspensionRange + wheelRadius) && hit.collider.transform.root != transform.root)
		{
			Vector3 velocityAtTouch = parent.GetPointVelocity(hit.point);
			
			float compression = hit.distance / (suspensionRange + wheelRadius);
			compression = -compression + 1;
			Vector3 force = -down * compression * suspensionForce;
			
			Vector3 t = transform.InverseTransformDirection(velocityAtTouch);
			t.z = t.x = 0;
			Vector3 shockDrag = transform.TransformDirection(t) * -suspensionDamp;
			
			float forwardDifference = transform.InverseTransformDirection(velocityAtTouch).z - speed;
			float newForwardFriction = Mathf.Lerp(forwardFriction, forwardSlipFriction, forwardSlip * forwardSlip);
			newForwardFriction = Mathf.Lerp(newForwardFriction, newForwardFriction * compression * 1, compressionFrictionFactor);
			if(frictionSmoothing > 0)
				usedForwardFriction = Mathf.Lerp(usedForwardFriction, newForwardFriction, Time.fixedDeltaTime / frictionSmoothing);
			else
				usedForwardFriction = newForwardFriction;
			Vector3 forwardForce = transform.TransformDirection(new Vector3(0, 0, -forwardDifference)) * usedForwardFriction;
			forwardSlip = Mathf.Lerp(forwardForce.magnitude / forwardSlipForce, forwardDifference / forwardSlipVelocity, forwardStiffnessFactor);
			
			float sidewaysDifference = transform.InverseTransformDirection(velocityAtTouch).x;
			float newSideFriction = Mathf.Lerp(sidewaysFriction, sidewaysSlipFriction, sideSlip * sideSlip);
			newSideFriction = Mathf.Lerp(newSideFriction, newSideFriction * compression * 1, compressionFrictionFactor);
			if(frictionSmoothing > 0)
				usedSideFriction = Mathf.Lerp(usedSideFriction, newSideFriction, Time.fixedDeltaTime / frictionSmoothing);
			else
				usedSideFriction = newSideFriction;
			Vector3 sideForce = transform.TransformDirection(new Vector3(-sidewaysDifference, 0, 0)) * usedSideFriction;
			sideSlip = Mathf.Lerp(sideForce.magnitude / sidewaysSlipForce, sidewaysDifference / sidewaysSlipVelocity, sidewaysStiffnessFactor);
			
			t = transform.InverseTransformDirection(velocityAtTouch);
			t.z = t.y = 0;
			Vector3 sideDrag = transform.TransformDirection(t) * -sidewaysDamp;
			
			parent.AddForceAtPosition(force + shockDrag + forwardForce + sideForce + sideDrag, hit.point);
			
			if(driven)
				car.AddForceOnMotor (forwardDifference * usedForwardFriction * Time.fixedDeltaTime);
			else
				speed += forwardDifference;	
			
			graphic.position = transform.position + (down * (hit.distance - wheelRadius));
		}
		else
		{
			graphic.position = transform.position + (down * suspensionRange);
		}
		
		graphic.transform.Rotate(360 * (speed / wheelCircumference) * Time.fixedDeltaTime, 0, 0); 
	}
	
}