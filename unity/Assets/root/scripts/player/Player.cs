﻿using UnityEngine;
using System.Collections;

public class Player : ISingleton<Player>
{
	private Transform mTransform;

	private float originalMaxAcceleration = 0;

	public void StopPlayer()
	{
		// kill velocity
	}

	public void boost(float acceleration){
		GetComponent<VehiclePhysics>().maxAcceleration = acceleration;
	}

	public void stopBoost(){
		GetComponent<VehiclePhysics>().maxAcceleration = originalMaxAcceleration;
	}

	public void WarpPlayer(Transform destination)
	{
		Debug.Log("Player.WarpPlayer");

		this.mTransform.position = destination.position;
		this.mTransform.rotation = destination.rotation;
	}

	#region Mono

	void Start()
	{
		this.mTransform = transform;
		originalMaxAcceleration = GetComponent<VehiclePhysics>().maxAcceleration;
	}

	#endregion

	#region Collider

	private void OnTriggerEnter(Collider c)
	{
		switch (c.gameObject.layer)
		{
			case UnityLayers.DEATH:
				Debug.Log("Player Respawn");
				GameObject spawn = GameObject.FindGameObjectWithTag(UnityTags.RESPAWN);
				this.StopPlayer();
				this.WarpPlayer(spawn.transform);
				break;

			default:
				Debug.LogWarning(string.Format("unhandled player collision with {0} layer", c.gameObject.layer));
				break;
		}
	}

	#endregion
}
