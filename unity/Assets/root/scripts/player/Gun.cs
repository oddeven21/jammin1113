﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour {

	// time to reload weapon
	public float reloadSpeed = 1;
	// current reload time
	private float reload = 1; 

	// trajectory configurations
	// speed of projectile
	public float power = 30;
	// angle drop off per second
	public float dropOff = 100;

	// burst effect
	public Transform burstEffect = null;

	// projectile resource
	public GameObject projectilePrototype = null;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		reload += Time.deltaTime;
		if (projectilePrototype && reload >= reloadSpeed && InputManager.GetAxis("Fire1") > 0) {
			GameObject projectile = (GameObject)Instantiate(projectilePrototype);
			projectile.transform.position = transform.position;
			projectile.transform.rotation = transform.rotation;
			projectile.GetComponent<Projectile>().speed = power;
			projectile.GetComponent<Projectile>().dropOff = dropOff;
			reload = 0;
			if (burstEffect != null) {
				burstEffect.particleSystem.Play();
			}
		} 
	}
}
