﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	public float speed = 0;
	public float dropOff = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(transform.forward * speed * Time.deltaTime, Space.World);
		transform.Rotate(transform.rotation.x + dropOff * Time.deltaTime, 0, 0);
	}

	void OnCollisionEnter (Collision check)
	{
		transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
		Destroy(transform.GetComponent<SphereCollider>());

		StartCoroutine(CommitSuicide(3));
	}

	// think delay seconds before committing suicide
	IEnumerator CommitSuicide(float delay)
	{
		yield return new WaitForSeconds(delay); //This does the magic
		Destroy(transform.gameObject);
	}
}
