﻿using UnityEngine;
using System.Collections;

[RequireComponent( typeof( Player ) ) ]
[RequireComponent( typeof( Rigidbody ) ) ]

public class Jumper : MonoBehaviour {

	public float jumpForce = 500.0f;
	private float distToGround;

	// Use this for initialization
	void Start () {
		distToGround = 0.5f;
	}

	void Jump() {
		rigidbody.AddRelativeForce( new Vector3( 0, jumpForce, 0 ) );
		Debug.Log ("Jump!");
	}

	bool IsGrounded() {
		return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
	}
	
	// Update is called once per frame
	void Update () {
		if ( InputManager.GetAxis ( "Jump" ) > 0 && IsGrounded() )
		{
			Jump();
		}
	
	}
}
