﻿using UnityEngine;
using System.Collections;

public class Boost : MonoBehaviour {

	public float cooldown = 10;
	public float cooldownPeriod = 10;

	public float acceleration = 300;
	// duration should be less than cooldown
	public float duration = 3;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		cooldown += Time.deltaTime;
		if (cooldown >= cooldownPeriod && InputManager.GetAxis("Boost") > 0) {
			GetComponent<Player>().boost(acceleration);
			cooldown = 0;
			StartCoroutine(TurnOff(duration));
		} 
	}
	
	IEnumerator TurnOff(float delay)
	{
		yield return new WaitForSeconds(delay);
		GetComponent<Player>().stopBoost();
	}
}
