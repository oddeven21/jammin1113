/*
 * This class is auto-generated do not modify (TagsLayersBuilder.cs) - blog.almostlogical.com
 * 11/24/2013 7:24:55 PM
 *
 */
public class UnityTags
{
	public const string UNTAGGED = "Untagged";
	public const string RESPAWN = "Respawn";
	public const string FINISH = "Finish";
	public const string EDITOR_ONLY = "EditorOnly";
	public const string MAIN_CAMERA = "MainCamera";
	public const string PLAYER = "Player";
	public const string GAME_CONTROLLER = "GameController";
}