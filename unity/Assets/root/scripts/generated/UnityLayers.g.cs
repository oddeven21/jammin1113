/*
 * This class is auto-generated do not modify (TagsLayersBuilder.cs) - blog.almostlogical.com
 * 11/24/2013 7:24:55 PM
 *
 */
public class UnityLayers
{
	public const int DEFAULT = 0;
	public const int TRANSPARENT_FX = 1;
	public const int IGNORE_RAYCAST = 2;
	public const int WATER = 3;
	public const int PLAYER = 8;
	public const int DEATH = 9;
	public const int PROJECTILE = 10;
	public const int COLLECTIBLE = 11;
}