﻿using UnityEngine;
using System.Collections;

public class WarpTrigger : MonoBehaviour
{
    public Transform WarpTo;

    private void Awake()
    {
    }

    private void OnTriggerEnter(Collider c)
    {
        StartCoroutine(this.WarpPlayer());
        this.collider.enabled = false;
    }

    private void OnTriggerExit(Collider c)
    {
    }

    private IEnumerator WarpPlayer()
    {
        float duration = 0.15f;

        InputManager.Enabled = false;
        Player.Instance.StopPlayer();

        CameraFade.Instance.FadeIn(duration);
        yield return new WaitForSeconds(duration);

        Player.Instance.WarpPlayer(this.WarpTo);

        CameraFade.Instance.FadeOut(duration);
        yield return new WaitForSeconds(duration);

        InputManager.Enabled = true;
        this.collider.enabled = true;
    }
}
