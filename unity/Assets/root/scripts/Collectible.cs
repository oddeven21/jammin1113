﻿using UnityEngine;
using System.Collections;

public class Collectible : MonoBehaviour
{
    private IEnumerator DelayedDestroy()
    {
        this.collider.enabled = false;
        this.renderer.enabled = false;

        yield return new WaitForSeconds(1);

        Object.Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider c)
    {
        switch (c.gameObject.layer)
        {
            case UnityLayers.PLAYER:
                if (this.audio != null)
                {
                    this.audio.Play();
                }

                StartCoroutine(this.DelayedDestroy());
                break;

            default:
                Debug.LogWarning(string.Format("unhandled collectible collision with {0} layer", c.gameObject.layer));
                break;
        }
    }
}
