﻿using UnityEngine;

public class CameraFade : ISingleton<CameraFade>
{
    public GUIStyle style = new GUIStyle();						// Style for background tiling
    public Texture2D fadeTexture;											// 1x1 pixel texture used for fading
    public Color currentColor = new Color(0, 0, 0, 0);			// default starting color: black and fully transparrent
    public Color targetColor = new Color(0, 0, 0, 0);			// default target color: black and fully transparrent
    public Color stepColor = new Color(0, 0, 0, 0);							// the delta-color is basically the "speed / second" at which the current color should change

    // Draw the texture and perform the fade:
    void OnGUI()
    {
        // If the current color of the screen is not equal to the desired color: keep fading!
        if (currentColor != targetColor)
        {
            // If the difference between the current alpha and the desired alpha is smaller than delta-alpha * deltaTime, then we're pretty much done fading:
            if (Mathf.Abs(currentColor.a - targetColor.a) < Mathf.Abs(stepColor.a) * Time.deltaTime)
            {
                currentColor = targetColor;
                SetScreenOverlayColor(currentColor);
                stepColor = new Color(0, 0, 0, 0);
            }
            else
            {
                // Fade!
                SetScreenOverlayColor(currentColor + stepColor * Time.deltaTime);
            }
        }

        // Only draw the texture when the alpha value is greater than 0:
        if (currentColor.a > 0)
        {
            GUI.depth = -1000;
            GUI.Label(new Rect(-10, -10, Screen.width + 10, Screen.height + 10), fadeTexture, style);
        }
    }

    void Awake()
    {
        this.fadeTexture = new Texture2D(1, 1);
        this.style.normal.background = this.fadeTexture;
    }

    private void SetScreenOverlayColor(Color newColor)
    {
        this.currentColor = newColor;
        this.fadeTexture.SetPixel(0, 0, this.currentColor);
        this.fadeTexture.Apply();
    }

    public void FadeIn(float fadeDuration)
    {
        this.currentColor = new Color(0, 0, 0, 0);
        this.SetScreenOverlayColor(this.currentColor);

        this.targetColor = Color.black;
        this.stepColor = (this.targetColor - this.currentColor) / fadeDuration;
    }

    public void FadeOut(float fadeDuration)
    {
        this.currentColor = Color.black;
        this.SetScreenOverlayColor(this.currentColor);

        this.targetColor = new Color(0, 0, 0, 0);
        this.stepColor = (this.targetColor - this.currentColor) / fadeDuration;
    }
}