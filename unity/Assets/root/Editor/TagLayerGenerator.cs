// Tags And Layers Builder - Auto Generate Tags and Layers classes containing consts for all variables for code completion - 2012-08-03
// released under MIT License
// http://www.opensource.org/licenses/mit-license.php
//
//@author		Devin Reimer - AlmostLogical Software
//@website 		http://blog.almostlogical.com
/*
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System;
using System.IO;
using UnityEditor;
using UnityEngine;

//Note: This class uses UnityEditorInternal which is an undocumented internal feature
public class TagsLayersBuilder : EditorWindow
{
    private const string FOLDER_LOCATION = "root/Scripts/generated/";
    private const string TAGS_FILE_NAME = "UnityTags";
    private const string LAYERS_FILE_NAME = "UnityLayers";
    private const string SCRIPT_EXTENSION = ".g.cs";

    [MenuItem("JAM/Rebuild Tags And Layers Classes")]
    static void RebuildTagsAndLayersClasses()
    {
        string folderPath = Application.dataPath + "/" + FOLDER_LOCATION;
        if (!Directory.Exists(folderPath))
        {
            Directory.CreateDirectory(folderPath);
        }
        File.WriteAllText(folderPath + TAGS_FILE_NAME + SCRIPT_EXTENSION, GetClassContentTags(TAGS_FILE_NAME, UnityEditorInternal.InternalEditorUtility.tags));
        File.WriteAllText(folderPath + LAYERS_FILE_NAME + SCRIPT_EXTENSION, GetClassContentLayers(LAYERS_FILE_NAME, UnityEditorInternal.InternalEditorUtility.layers));
        AssetDatabase.ImportAsset("Assets/" + FOLDER_LOCATION + TAGS_FILE_NAME + SCRIPT_EXTENSION, ImportAssetOptions.ForceUpdate);
        AssetDatabase.ImportAsset("Assets/" + FOLDER_LOCATION + LAYERS_FILE_NAME + SCRIPT_EXTENSION, ImportAssetOptions.ForceUpdate);
    }

    private static string GetClassContentLayers(string className, string[] labelsArray)
    {
        string output = "";
        output += "/*\n";
        output += " * This class is auto-generated do not modify (TagsLayersBuilder.cs) - blog.almostlogical.com\n";
        output += " * " + DateTime.Now.ToString() + "\n";
        output += " *\n";
        output += " */\n";
        output += "public class " + className + "\n";
        output += "{\n";
        int value = 0;
        for (int i = 0; i < labelsArray.Length; ++i)
        {
            string varName = ToUpperCaseWithUnderscores(labelsArray[i]);
            output += "\t" + "public const int " + varName + " = " + value + ";\n";
            ++value;

            if (i == 3)
                value = 8;
        }
        output += "}";
        return output;
    }

    private static string GetClassContentTags(string className, string[] labelsArray)
    {
        string output = "";
        output += "/*\n";
        output += " * This class is auto-generated do not modify (TagsLayersBuilder.cs) - blog.almostlogical.com\n";
        output += " * " + DateTime.Now.ToString() + "\n";
        output += " *\n";
        output += " */\n";
        output += "public class " + className + "\n";
        output += "{\n";
        foreach (string label in labelsArray)
        {
            string varName = ToUpperCaseWithUnderscores(label);
            string value = label;

            output += "\t" + "public const string " + varName + " = " + '"' + value + '"' + ";\n";
        }
        output += "}";
        return output;
    }

    private static string ToUpperCaseWithUnderscores(string input)
    {
        string output = "" + input[0];

        input = input.Replace(' ', '_');

        for (int n = 1; n < input.Length; n++)
        {
            //if ((char.IsUpper(input[n]) || input[n] == ' ') && !char.IsUpper(input[n - 1]) && input[n - 1] != '_' && input[n - 1] != ' ')
            if (char.IsUpper(input[n]) && input[n - 1] != '_' && input[n] != '_' && !char.IsUpper(input[n - 1]))
            {
                output += "_";
            }

            output += input[n];

            //if (input[n] != ' ' && input[n] != '_')
            //{
            //    output += input[n];
            //}
        }

        output = output.ToUpper();
        return output;
    }
}